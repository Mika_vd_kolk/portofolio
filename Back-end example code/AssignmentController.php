<?php

namespace App\Http\Controllers;

use App\Http\Resources\AssignmentResource;
use App\Models\Courses\Assignment;
use App\Http\Requests\StoreAssignmentRequest;
use App\Http\Requests\UpdateAssignmentRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {
            return response(AssignmentResource::collection(Assignment::get()), Response::HTTP_ACCEPTED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAssignmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAssignmentRequest $request)
    {
        $user = Auth::user();
        if($user->hasAccess('edit_course')) {

            $assignment = Assignment::create([
                'assignment' => $request->input('assignment'),
                'course_id' => $request->input('course_id'),
            ]);

            return response(new AssignmentResource($assignment), Response::HTTP_CREATED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Courses\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {
            $assignment = Assignment::find($id);
            return response(new AssignmentResource($assignment), Response::HTTP_CREATED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAssignmentRequest  $request
     * @param  \App\Models\Courses\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAssignmentRequest $request, $id)
    {
        $user = Auth::user();
        if($user->hasAccess('edit_course')) {
            $assignment = Assignment::find($id);
            $assignment->update($request->only('assignment'));

            return response(new AssignmentResource($assignment), Response::HTTP_ACCEPTED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Courses\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if($user->hasAccess('edit_course')) {
            $assignment = Assignment::find($id);
            $assignment->delete();
            return response(null, Response::HTTP_ACCEPTED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    public function storeGitlabUrl(StoreAssignmentRequest $request, $id)
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {
            $assignment = Assignment::find($id);
            $hasGitlab = $assignment->user()->where('user_id', Auth::id())->exists();
            if($hasGitlab == false){
                $assignment->user()->attach(Auth::id(), ['gitlab' => $request->gitlab]);
            }
            else{
                return response(null, Response::HTTP_ALREADY_REPORTED);
            }

            return response(null, Response::HTTP_CREATED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    public function showUserAssignment($answers, $id = 0)
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {

            if($answers == 'user'){
                if($id == 0)$id = $user->id;
                $result = DB::table('assignment_user')
                    ->where('user_id', '=', $id)
                    ->get();
                return $result->toJson();
            }
            elseif($answers == 'assignment'){
                $result = DB::table('assignment_user')
                    ->where('assignment_id', '=',$id)
                    ->get();
                return $result->toJson();
            }
            elseif($answers == 'all'){
                $result = DB::table('assignment_user')
                    ->get();
                return $result->toJson();
            }
            else{
                return response(Response::HTTP_BAD_REQUEST);
            }

        }
        return response(Response::HTTP_FORBIDDEN);
    }
    public function storeFeedback(StoreAssignmentRequest $request, $id)
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {
            $assignment = Assignment::find($id);
            $hasGitlab = $assignment->user()->where('user_id', Auth::id())->exists();
            if($hasGitlab == false){

                return response(null, Response::HTTP_BAD_REQUEST);
            }
            else{
                $assignment->user()->updateExistingPivot(Auth::id(), ['feedback' => $request->feedback,'grade' => $request->grade]);
            }

            return response(null, Response::HTTP_CREATED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }
}
