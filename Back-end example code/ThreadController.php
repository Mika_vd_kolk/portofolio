<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreThreadRequest;
use App\Http\Requests\UpdateInfoRequest;
use App\Http\Requests\UpdateThreadRequest;
use App\Http\Resources\CourseResource;
use App\Http\Resources\ThreadCollection;
use App\Http\Resources\ThreadResource;
use App\Models\forum\Post;
use App\Models\forum\Thread;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Courses\Course;
use App\Http\Resources\Post as PostResource;

class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $threads = Course::with(['threads', 'threads.user']);
        return CourseResource::collection($threads->paginate(50));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreThreadRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreThreadRequest $request)
    {
        Gate::authorize('view', 'users');

        $thread = new Thread;

        $thread->description = $request->description;
        $thread->title = $request->title;
        $thread->course_id = $request->course_id;
        $thread->user_id = Auth::user()->id;
        $thread->save();

        return response(new ThreadResource($thread), Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\forum\Thread $thread
     * @return \Illuminate\Http\Response
     */
    public function show(Thread $thread)
    {
        $thread->load(['posts', 'posts.user', 'user']);
        return new ThreadResource($thread);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateThreadRequest $request
     * @param \App\Models\forum\Thread $thread
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateThreadRequest $request, $id)
    {
        $thread = Thread::find($id);

        $thread->update($request->only('title', 'description'));

        return response(new ThreadResource($thread), Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\forum\Thread $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();

        if ($user->hasRole(['Admin', 'Teacher'])) {
            Thread::destroy($id);
            return response(null, Response::HTTP_NO_CONTENT);
        } else {
            $thread = Thread::where('user_id', $user->id)->where('id', $id)->firstOrFail();
            Thread::destroy($id);
            return response(null, Response::HTTP_NO_CONTENT);
        }
    }

    public function close(UpdateThreadRequest $request, $id)
    {
        $thread = Thread::findOrFail($id);

        $thread->closedAt = new DateTime();
        $thread->save();

        return response(new ThreadResource($thread), Response::HTTP_ACCEPTED);
    }
}
