<?php

namespace App\Http\Controllers;

use App\Http\Resources\CourseResource;
use App\Models\Courses\Course;
use App\Http\Requests\StoreCoursesRequest;
use App\Http\Requests\UpdateCoursesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CourseController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {

            if($request->trashed && $request->trashed == 'true' && $user->hasRole(['Teacher', 'Admin'])) $courses = Course::withTrashed()->get();
            else $courses = Course::withCount(['video'])->get();

            return response(CourseResource::collection($courses), Response::HTTP_OK);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    public function store(StoreCoursesRequest $request)
    {
        $user = Auth::user();
        if ($user->hasAccess('edit_course')) {

            $file = $request->file('image');
            $name = $file->getClientOriginalName();
            $file->storeAs('public/uploads/images', $file->getClientOriginalName());

            $course = Course::create([
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'image' => 'uploads/images/' . $name,
            ]);

            return response(new CourseResource($course), Response::HTTP_CREATED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    public function show($id)
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {

            $course = Course::with(['assignment','user','video' => function ($q){
                $q->orderBy('order');
            }])->find($id);
            return response(new CourseResource($course), Response::HTTP_CREATED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    public function update(UpdateCoursesRequest $request, $id)
    {
        $user = Auth::user();
        if ($user->hasAccess('edit_course')) {
            $course = Course::find($id);
            if (!empty($request->file('image'))) {
                $file = $request->file('image');
                $filePath = $file->storeAs('public/uploads/images', $file->getClientOriginalName());
                $course->image = $filePath;
            }
            $course->update($request->only('title', 'description'));

            return response(new CourseResource($course), Response::HTTP_ACCEPTED);
        }
        return response(Response::HTTP_FORBIDDEN);

    }

    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->hasAccess('edit_course')) {
            $course = Course::find($id);
            $course->delete();
            return response(null, Response::HTTP_ACCEPTED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    public function setCourseStatus($id)
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {
            $course = Course::with('video', 'assignment')->find($id);
            $hasTask = $course->user()->where('user_id', Auth::id())->exists();
            if($hasTask == false){
                $course->user()->attach(Auth::id());
            }
            else{
                $course->user()->updateExistingPivot(Auth::id(), [
                    'status' => 'completed',
                ]);
            }
            return response(null, Response::HTTP_CREATED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }
}
