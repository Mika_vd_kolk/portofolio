<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UpdatePasswordRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function index()
    {
        Gate::authorize('view', 'users');
        //! Use with('role') to get the user back with the role attached
        return UserResource::collection(User::with('role')->paginate());
    }


    public function store(UserCreateRequest $request)
    {
        Gate::authorize('edit', 'users');

        $user = User::create(
            $request->only('first_name', 'last_name', 'email', 'role_id') + ['password' => Hash::make('welcome123')]
        );

        return response(new UserResource($user), Response::HTTP_CREATED);
    }


    public function show($id)
    {
        Gate::authorize('view', 'users');

        return new UserResource(User::with('role')->find($id));
    }


    public function update(Request $request, $id)
    {
        Gate::authorize('edit', 'users');

        $user = User::find($id);

        $user->update($request->only('first_name', 'last_name', 'email', 'git_user_name'));

        return response(new UserResource($user), Response::HTTP_ACCEPTED);
    }


    public function destroy($id)
    {
        Gate::authorize('edit', 'users');

        User::destroy($id);

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
