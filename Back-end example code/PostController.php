<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\Post as ResourcesPost;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\UserResource;
use App\Http\Resources\PostCollection;
use App\Models\forum\Post;
use App\Http\Resources\PostResource;
use Illuminate\Contracts\Auth\Access\Gate as AccessGate;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Whoops\Run;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(['thread']);
        return ResourcesPost::collection($posts->paginate(50));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        Gate::authorize('view', 'User');
        $post = new Post;

        $post->description = $request->description;
        $post->thread_id = $request->thread_id;
        $post->user_id = Auth::user()->id;
        $post->save();

        return response(new ResourcesPost($post), Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\forum\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return new ResourcesPost($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\forum\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePostRequest  $request
     * @param  \App\Models\forum\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        if ($post->user_id == Auth::id()){

            $post->update($request->only('description'));
            $post->save();

            return response(new ResourcesPost($post), Response::HTTP_ACCEPTED);
        }
        return response('', Response::HTTP_UNAUTHORIZED);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\forum\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //dd($post);
        if($post->user_id == Auth::id() || Auth::user()->hasRole(['Admin', 'Teacher'])){

            Post::destroy($post);

            return response(null, Response::HTTP_NO_CONTENT);
        }
        return response('', Response::HTTP_UNAUTHORIZED);

    }
}
