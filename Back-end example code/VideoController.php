<?php

namespace App\Http\Controllers;

use App\Http\Resources\VideoResource;
use App\Models\Courses\Video;
use App\Http\Requests\StoreVideoRequest;
use App\Http\Requests\UpdateVideoRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class VideoController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {
            return response(VideoResource::collection(Video::all()), Response::HTTP_ACCEPTED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    public function store(StoreVideoRequest $request)
    {
        $user = Auth::user();
        if($user->hasAccess('edit_course')) {

            $url = $request->input('url'); //pass your dynamic url here
            parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
            $url = $my_array_of_vars['v'];

            Storage::disk('local')->put('public/uploads/thumbnails/' . $url . '.jpg', file_get_contents('https://img.youtube.com/vi/' . $url . '/hqdefault.jpg'));

            $test = Video::where('course_id', '=', $request->input('course_id'))->get();
            foreach ($test as $test2){
                $array[] = $test2->order;
            }
            if (isset($array)) {
                $order = max($array) + 1;
            }
            else{
                $order = 1;
            }

            $video = Video::create([
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'thumbnail' => 'uploads/thumbnails/' . $url . '.jpg',
                'order' => $order,
                'url' => 'https://www.youtube.com/embed/' . $url,
                'course_id' => $request->input('course_id'),
            ]);

            return response(new VideoResource($video), Response::HTTP_CREATED);
        }
        return response(Response::HTTP_FORBIDDEN);

    }

    public function show($id)
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {
            $video = Video::with('user')->find($id);
            return response (new VideoResource($video), Response::HTTP_ACCEPTED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    public function update(UpdateVideoRequest $request, Video $video)
    {
        $user = Auth::user();
        if($user->hasAccess('edit_course')) {

            if(!empty($request->input('url'))) {
                $url = $request->input('url'); //pass your dynamic url here
                parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
                $url = $my_array_of_vars['v'];

                Storage::disk('local')->put('public/uploads/thumbnails/' . $url . '.jpg', file_get_contents('https://img.youtube.com/vi/' . $url . '/hqdefault.jpg'));
                $video->thumbnail = 'uploads/thumbnails/' . $url . '.jpg';
                $video->url = 'https://www.youtube.com/embed/' . $url;
            }
            $video->update($request->only('title', 'description'));

            return response(new VideoResource($video), Response::HTTP_CREATED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    public function destroy($id)
    {
        $user = Auth::user();
        if($user->hasAccess('edit_course')) {
            $course = Video::find($id);
            $course->delete();
            return response(null, Response::HTTP_ACCEPTED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }

    public function setVideoStatus($id)
    {
        $user = Auth::user();
        if($user->hasAccess('view_course')) {
            $video = Video::find($id);
            $hasStatus = $video->user()->where('user_id', Auth::id())->exists();
            if($hasStatus == false){
                $video->user()->attach(Auth::id());
            }
            else{
                $video->user()->updateExistingPivot(Auth::id(), [
                    'status' => 'completed',
                ]);
            }
            return response(null, Response::HTTP_CREATED);
        }
        return response(Response::HTTP_FORBIDDEN);
    }
}
