import React, {useState, useEffect} from "react";
import PropTypes from 'prop-types'

function Button({children, onClick, size}) {
    return (
            <button
                onClick={onClick}
                className={`button ${size}`}
            >{children}
            </button>
    );
}

Button.propTypes = {
    onClick: PropTypes.func.isRequired,
    size: PropTypes.oneOf(['sm', 'xl']),
}
export default Button;
