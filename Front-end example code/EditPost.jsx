import axios from "axios";
import React, {useEffect, useState} from "react";
import {isAuthorized} from "../utils";

function EditPost({setThread, post, setReadMode, user}) {
    const [description, setDescription] = useState("");

    useEffect(() => {
        setDescription(post.description);
    }, [post]);

    const updatePost = async (e) => {
        const postResponse = await axios.put(`post/${post.id}`, {
            description
        });
        setThread(prevThread => {
            const postIndex = prevThread.posts.findIndex(o => o.id === post.id);
            prevThread.posts[postIndex].description = description;
            return prevThread;
        });
        setReadMode();
    };

    return (
        <div className="bg-[#F7F7FF] rounded shadow-md py-5 px-10">
            <textarea
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                className="text-sm text-gray-500 border-2 border-violet-300 focus:border-purple-600 w-full"
                rows="8"
                cols="40"
            />
            <br/>
            <hr className="border-violet-300"/>
            <h2 className="uppercase text-sm text-indigo-500">
                {post.user.first_name}&nbsp;{post.user.last_name}
            </h2>

            <div className="flex justify-end">
                {user.id === post.user.id &&
                    <button
                        onClick={updatePost}
                        className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-500 text-base font-medium text-white hover:bg-indigo-600 sm:ml-3 sm:w-auto sm:text-sm"
                    >
                        Opslaan
                    </button>
                }
                {(user.id === post.user.id || isAuthorized(user)) &&
                    <button
                        onClick={setReadMode}
                        className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-500 text-base font-medium text-white hover:bg-red-600 sm:ml-3 sm:w-auto sm:text-sm">
                        Annuleren
                    </button>
                }
            </div>
        </div>
    )

}

export default EditPost;
