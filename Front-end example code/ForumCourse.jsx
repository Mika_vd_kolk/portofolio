import React, {useState, useEffect} from "react";
import PropTypes from 'prop-types'
import {Link} from "react-router-dom";

function ForumCourse(props) {
    const { threads } = props.course;
    const lastThread = threads.length > 0 ? threads[threads.length - 1].title : "Geen threads";

return (
    <div className="container mx-auto max-w-6xl">
        <div className="grid grid-flow-col bg-white shadow-lg rounded-lg mt-2 p-10">
            <div className="row grid grid-flow-col grid-cols-3 text-center">
                <div className="col-4 text-3xl">
                    <Link className="hover:font-bold hover:text-indigo-500" to={"/forum/"+props.course.id}>{props.course.title}</Link>
                </div>
                <div className="col-4">
                    {props.course.threads.length}
                </div>
                <div className="col-4">
                    {threads.length > 0 ?
                    <Link className="hover:text-indigo-500" to={"/forum/"+props.course.id+"/"+threads[threads.length - 1].id}>{lastThread}</Link> : <span>Geen threads</span>
                    }
                </div>
            </div>
        </div>
    </div>
)

}

export default ForumCourse;
