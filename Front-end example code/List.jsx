import React, {useState, useEffect} from "react";
import PropTypes from "prop-types";

function List({template, dataList}) {
    const Tpl = template
    const items = dataList.map(item => {
        return (
            <div
                key={item.key}
                className="w-full h-20 align-middle border border-solid border-gray-400"
            >
                <Tpl {...item}></Tpl>
            </div>
        )
    })

    return(
        <div
            className="flex items-center flex-col"
        >
            {items}
        </div>
    )
}

List.propTypes = {
    template: PropTypes.func.isRequired,
    dataList: PropTypes.array.isRequired,
}

export default List
