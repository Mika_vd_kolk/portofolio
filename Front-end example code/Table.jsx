import React, {} from "react";
import PropTypes from 'prop-types'

function Table({dataList, columns, className, onRowClick}) {
    const headers = Object.keys(columns).map((column, index) => {
        return (
            <th className="
                py-4 pl-7 last:pr-7 text-left
            " key={index}>{columns[column].display ?? column}</th>
        )
    })
    const rows = dataList.map((item, rowNum) => {
        const cols = Object.keys(columns).map((column, i) => {
            let content = item[column]

            if(columns[column].render) {
                const Tpl = columns[column].render
                content = <Tpl {...item[column]}>{!(typeof content === 'object') && content}</Tpl>
            }

            return (
                <td className={columns[column].className} style={{height: "inherit"}} key={column}>
                    <div className="">{content}</div>
                </td>
            )
        })
        return (
            <tr onClick={() => onRowClick(item)} className="h-px hover:bg-gray-100 bg-white group
            " key={rowNum}>
                {cols}
            </tr>

        )
    })
    return (
        <div className={`rounded-lg overflow-x-auto ${className}`}>
            <table className="table-auto w-full table">
                <thead className="bg-gray-200">
                <tr className="px-7">{headers}</tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>
        </div>
    );
}

// Table.propTypes = {
//     dataList: PropTypes.array.isRequired,
//     columns: PropTypes.object.isRequired,
//     className: PropTypes.string,
// }

export default Table
