import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import PropTypes from "prop-types";
// import ForumItem from "./ForumItem";
import { Link } from "react-router-dom";
import axios from "axios";
import CustomModal from "./Modal";
import { data } from "autoprefixer";

export default function CourseWrapper({ randomCourse }) {
    const [course, setCourse] = useState({});
    const [password, setPassword] = useState("");
    const [showModal, setShowModal] = useState(false);
    const [assignment, setAssignment] = useState("");
    const [redirect, setRedirect] = useState(false);
    let [videoId, setVideoId] = useState([]);
    const navigate = useNavigate();
    const { id } = useParams();
    console.log(id);

    useEffect(() => {
        const getCourse = async () => {
            const { data } = await axios.get(`course/${id}`);
            setCourse(data);
        };

        getCourse();
    }, []);

    // return (
    //     <div>
    //         <h3>ID: {id}</h3>
    //     </div>
    const handleSubmit = async (e) => {
        e.preventDefault();

        function submitAssignment() {
            axios.post(`storeGitlabUrl/${course.assignments[0].id}`);
        }
        console.log(randomCourse);
    };

    function submitAssignment(e) {
        e.preventDefault();

        axios.post(`storeGitlabUrl/${course.assignments[0].id}`, {
            gitlab: assignment,
        });
    }
    if (showModal === true) {
        return (
            <CustomModal
                visible={showModal}
                onClose={() => setShowModal(false)}
                videoId={videoId}
            />
        );
    }

    return (
        <div className={"grid grid-cols-3 h-screen"}>
            <div className={"flex flex-col col-span-2"}>
                <div className={"flex flex-col"}>
                    <h1 className="font-medium leading-tight text-5xl mt-0 mb-2 text-black mx-8 my-8 mt-9">
                        {course.title}
                    </h1>
                </div>

                <div className={"flex flex-col"}>
                    <img
                        src="https://mdbootstrap.com/img/new/slides/041.jpg"
                        className="max-w-full h-auto rounded-lg shadow-lg bg-white mx-8 mb-8"
                        alt="..."
                    />
                </div>
                <div>
                    <div className={"flex flex-col"}>
                        <h2 className="font-medium leading-tight text-4xl mt-0 mb-2 text-black mx-8 my-8 col-span-2">
                            Video's
                        </h2>
                    </div>
                    <div className={"grid grid-cols-4 h-screen"}>
                        {course.videos &&
                            course.videos.map((video) => (
                                <div key={video.id}>
                                    <div className="flex justify-center">
                                        <div className="rounded-lg shadow-lg bg-white max-w-sm mx-8 mb-8">
                                            <a href="#!">
                                                <img
                                                    className="rounded-t-lg"
                                                    src="https://picsum.photos/300/300"
                                                    alt="house"
                                                />
                                            </a>
                                            <button
                                                onClick={() => {
                                                    setShowModal(true);
                                                    setVideoId(video.id);
                                                }}
                                            >
                                                <div className="p-6 ">
                                                    <h5 className="text-gray-900 text-xl font-medium mb-2">
                                                        {video.title}
                                                    </h5>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            ))}
                    </div>
                </div>
            </div>

            <div className={"flex flex-col"}>
                <div className="container max-w-full">
                    <div className={"flex flex-col"}>
                        <h2 className="font-medium leading-tight text-4xl mt-0 mb-2 text-black mx-8 my-8 mt-12">
                            Beschrijving
                        </h2>
                    </div>
                    <div className="flex justify-center">
                        <div className="block p-6 rounded-lg shadow-lg bg-white mx-8 mb-8">
                            <p className="text-gray-700 text-base mb-4">
                                {course.description}
                            </p>
                        </div>
                    </div>
                </div>

                {course.assignments && (
                    <div className={"flex flex-col"}>
                        <div className="container max-w-full">
                            <div className={"flex flex-col"}>
                                <h2 className="font-medium leading-tight text-4xl mt-0 mb-2 text-black mx-8 my-8">
                                    Opdracht
                                </h2>
                            </div>
                            <div className="flex justify-center">
                                <div className="block p-6 rounded-lg shadow-lg bg-white mx-8 mb-8">
                                    <p className="text-gray-700 text-base mb-4">
                                        {course.assignments[0].assignment}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
                {course.assignments && (
                    <div className={"flex flex-col"}>
                        <div className="flex justify-center">
                            <div className="mb-3 xl:w-96">
                                <h5 className="font-medium leading-tight text-xl mt-0 mb-2 text-black">
                                    Inleverpunt
                                </h5>

                                <section className="mt-0">
                                    <form
                                        id="gitlab_form"
                                        className="flex flex-col"
                                        onSubmit={submitAssignment}
                                    >
                                        <div className="mb-6 pt-3 rounded ">
                                            <input
                                                placeholder="Gitlab URL"
                                                type="assigment"
                                                value={assignment}
                                                required
                                                className="w-full text-indigo-200 placeholder-indigo-200 focus:outline-none border-b border-violet-300 focus:border-purple-600 transition duration-500 px-3 pb-3"
                                                onChange={(e) =>
                                                    setAssignment(
                                                        e.target.value
                                                    )
                                                }
                                            />
                                        </div>
                                    </form>
                                </section>
                            </div>
                        </div>
                        <div>
                            <div className="flex justify-center">
                                <button
                                    onClick={submitAssignment}
                                    type="button"
                                    className="inline-block px-6 py-2.5 bg-blue-600
                            text-white font-medium text-xs leading-tight uppercase
                            rounded-full shadow-md hover:bg-blue-700 hover:shadow-lg
                            focus:bg-blue-700 focus:shadow-lg focus:outline-none
                            focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                                >
                                    Inleveren
                                </button>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}

// export default CoursesWrapper;
