import React, {useState, useEffect} from "react";
import PropTypes from 'prop-types'
import {SearchIcon} from '@heroicons/react/solid'

function Input({placeHolder, onChange, prepend, className}) {
    return (
        <div className={`mt-1 relative rounded-md shadow-sm ${className}`}>
            {prepend &&
                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none py">
                    <span className="sm:text-sm">{React.cloneElement(prepend, {className: "h-5 w-5 text-indigo-500 "})}</span>
                </div>
            }
            <input
                onChange={onChange}
                type="text"
                className={`focus:ring-indigo-500 focus:border-indigo-500 block w-full ${prepend ? 'pl-8' : 'pl-3'} pr-3 sm:text-sm border-gray-300 rounded-md`}
                placeholder={placeHolder}
            />
        </div>
    );
}

Input.propTypes = {
    prepend: PropTypes.object,
    placeHolder: PropTypes.string,
    className: PropTypes.string,
    onChange: PropTypes.func.isRequired
}

export default Input
