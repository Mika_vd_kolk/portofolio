import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";
import { AdjustmentsIcon, HomeIcon } from "@heroicons/react/outline";
import { ChatAlt2Icon } from "@heroicons/react/outline";
import { UserContext } from "./Wrapper";

export default function Nav() {
    const user = useContext(UserContext);

    const logout = async () => {
        await axios.post("logout", {});
    };

    let activeStyle = {
        fill: "#6366f1",
    };

    return (
        <nav className="fixed top-0 left-0 h-screen min-h-full w-20 m-0 flex flex-col bg-white text-black shadow-lg">
            <img
                className="relative flex items-center justify-center h-13 w-13 mt-6 mb-5 mx-auto"
                src="img/logo.svg"
                alt="test"
            />
            <NavLink
                className="relative flex items-center justify-center h-12 w-12 mt-2 mb-2 mx-auto"
                to={"/"}
                style={({ isActive }) => (isActive ? activeStyle : undefined)}
            >
                <svg width="24" height="23.986" viewBox="0 0 24 23.986">
                    <path
                        d="M23.121,9.069,15.536,1.483a5.008,5.008,0,0,0-7.072,0L.879,9.069A2.978,2.978,0,0,0,0,11.19v9.817a3,3,0,0,0,3,3H21a3,3,0,0,0,3-3V11.19A2.978,2.978,0,0,0,23.121,9.069ZM15,22.007H9V18.073a3,3,0,0,1,6,0Zm7-1a1,1,0,0,1-1,1H17V18.073a5,5,0,0,0-10,0v3.934H3a1,1,0,0,1-1-1V11.19a1.008,1.008,0,0,1,.293-.707L9.878,2.9a3.008,3.008,0,0,1,4.244,0l7.585,7.586a1.008,1.008,0,0,1,.293.7Z"
                        transform="translate(0 -0.021)"
                    />
                </svg>
            </NavLink>

            <NavLink
                className="relative flex items-center justify-center h-12 w-12 mt-2 mb-2 mx-auto"
                to={"/courses"}
                style={({ isActive }) => (isActive ? activeStyle : undefined)}
            >
                <svg width="24" height="22" viewBox="0 0 24 22">
                    <path
                        d="M22.2,2.163a4.992,4.992,0,0,0-4.1-1.081l-3.822.694A4,4,0,0,0,12,3.065,4,4,0,0,0,9.716,1.776L5.9,1.082A5,5,0,0,0,0,6V16.793a5,5,0,0,0,4.1,4.919l6.286,1.143a9,9,0,0,0,3.218,0L19.9,21.712A5,5,0,0,0,24,16.793V6a4.983,4.983,0,0,0-1.8-3.837ZM11,20.928c-.084-.012-.168-.026-.252-.041L4.463,19.745A3,3,0,0,1,2,16.793V6A3,3,0,0,1,5,3a3.081,3.081,0,0,1,.54.049l3.82.7A2,2,0,0,1,11,5.712Zm11-4.135a3,3,0,0,1-2.463,2.952l-6.285,1.142c-.084.015-.168.029-.252.041V5.712a2,2,0,0,1,1.642-1.968l3.821-.7A3,3,0,0,1,22,6Z"
                        transform="translate(0 -1)"
                    />
                </svg>
            </NavLink>

            {/* <NavLink
                className="relative flex items-center justify-center h-12 w-12 mt-2 mb-2 mx-auto"
                to={"/makeCourse"}
                style={({ isActive }) => (isActive ? activeStyle : undefined)}
            >
                <svg width="24.005" height="23" viewBox="0 0 24.005 23">
                    <path
                        d="M22.9,8.955a1.987,1.987,0,0,0-2.092.184L18.975,10.5A5.007,5.007,0,0,0,14,6h-.085L9.793,1.879A2.976,2.976,0,0,0,7.672,1H1A1,1,0,0,0,1,3H7.672a1.006,1.006,0,0,1,.707.293L11.086,6H5a5.006,5.006,0,0,0-5,5v8a5.006,5.006,0,0,0,5,5h9a5.007,5.007,0,0,0,4.975-4.5l1.83,1.364a2,2,0,0,0,3.2-1.6V10.743A1.988,1.988,0,0,0,22.9,8.955ZM17,19a3,3,0,0,1-3,3H5a3,3,0,0,1-3-3V11A3,3,0,0,1,5,8h9a3,3,0,0,1,3,3Zm5,.257-3-2.236V12.979l3-2.236Z"
                        transform="translate(0 -1)"
                    />
                </svg>
            </NavLink> */}

            {/* <NavLink
                className="relative flex items-center justify-center h-12 w-12 mt-2 mb-2 mx-auto"
                to={"/category"}
                style={({ isActive }) => (isActive ? activeStyle : undefined)}
            >
                <svg width="26" height="26" viewBox="0 0 20 24">
                    <path
                        d="M17,0H7A5.006,5.006,0,0,0,2,5V20a4,4,0,0,0,4,4H17a5.006,5.006,0,0,0,5-5V5A5.006,5.006,0,0,0,17,0Zm3,5V16H8V2h9A3,3,0,0,1,20,5ZM6,2.172V16a3.98,3.98,0,0,0-2,.537V5A3,3,0,0,1,6,2.172ZM17,22H6a2,2,0,0,1,0-4H20v1A3,3,0,0,1,17,22Z"
                        transform="translate(-2)"
                    />
                </svg>
            </NavLink> */}

            <NavLink
                className="relative flex items-center justify-center h-12 w-12 mt-2 mb-2 mx-auto"
                to={"/forum"}
                style={({ isActive }) => (isActive ? activeStyle : undefined)}
            >
                <ChatAlt2Icon width="30" height="30" viewBox="0 0 24 24" />
            </NavLink>

            {user && user.role === "ADMIN" && (
                <NavLink
                    className="relative flex items-center justify-center h-12 w-12 mt-2 mb-2 mx-auto"
                    to={"/admin"}
                >
                    <AdjustmentsIcon className="h-8 w-8" />
                </NavLink>
            )}

            {/* <NavLink
                to={"/profile"}
                className="relative flex items-center justify-center h-12 w-12 mt-auto mb-2 mx-auto"
                style={({ isActive }) => (isActive ? activeStyle : undefined)}
            >
                <svg width="24" height="24" viewBox="0 0 24 24">
                    <path d="M7.5,13A4.5,4.5,0,1,1,12,8.5,4.5,4.5,0,0,1,7.5,13Zm0-7A2.5,2.5,0,1,0,10,8.5,2.5,2.5,0,0,0,7.5,6ZM15,23v-.5a7.5,7.5,0,0,0-15,0V23a1,1,0,0,0,2,0v-.5a5.5,5.5,0,0,1,11,0V23a1,1,0,0,0,2,0Zm9-5a7,7,0,0,0-11.667-5.217,1,1,0,1,0,1.334,1.49A5,5,0,0,1,22,18a1,1,0,0,0,2,0ZM17.5,9A4.5,4.5,0,1,1,22,4.5,4.5,4.5,0,0,1,17.5,9Zm0-7A2.5,2.5,0,1,0,20,4.5,2.5,2.5,0,0,0,17.5,2Z" />
                </svg>
            </NavLink> */}

            <div className="relative flex flex-col my-10 gap-3 items-center justify-center h-12 w-12 mt-auto mx-auto">
                {/* <NavLink
                    to={"/profile"}
                    className="relative flex items-center justify-center h-12 w-12 mt-auto mb-2 mx-auto"
                >
                    Profiel
                </NavLink> */}

                <NavLink
                    to={"/profile"}
                    className="relative flex items-center justify-center h-12 w-12 mt-auto mb-2 mx-auto"
                    style={({ isActive }) =>
                        isActive ? activeStyle : undefined
                    }
                >
                    <svg width="24" height="24" viewBox="0 0 24 24">
                        <path d="M7.5,13A4.5,4.5,0,1,1,12,8.5,4.5,4.5,0,0,1,7.5,13Zm0-7A2.5,2.5,0,1,0,10,8.5,2.5,2.5,0,0,0,7.5,6ZM15,23v-.5a7.5,7.5,0,0,0-15,0V23a1,1,0,0,0,2,0v-.5a5.5,5.5,0,0,1,11,0V23a1,1,0,0,0,2,0Zm9-5a7,7,0,0,0-11.667-5.217,1,1,0,1,0,1.334,1.49A5,5,0,0,1,22,18a1,1,0,0,0,2,0ZM17.5,9A4.5,4.5,0,1,1,22,4.5,4.5,4.5,0,0,1,17.5,9Zm0-7A2.5,2.5,0,1,0,20,4.5,2.5,2.5,0,0,0,17.5,2Z" />
                    </svg>
                </NavLink>

                <NavLink
                    className="relative flex items-center justify-center h-12 w-12 mt-2 mb-2 mx-auto"
                    to={"/login"}
                    onClick={logout}
                >
                    Logout
                </NavLink>
            </div>
        </nav>
    );
}
