import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Course } from "../models/course";
import { useNavigate } from "react-router-dom";

function CourseCard({ course }) {
    const [opened, setOpen] = useState(false);
    const [openedf, setOpenf] = useState(false);
    const [CourseWrapper, setCourseWrapper] = useState(false);
    const [redirect, setRedirect] = useState(false);
    const navigate = useNavigate();

    function toggleCard() {
        if (!opened) {
            setOpenf(!openedf);
            setTimeout(() => {
                setOpen(!opened);
            }, 500);
        } else {
            setOpen(!opened);
            setOpenf(!openedf);
        }
    }

    // function openCourse(datalist) {
    //     navigate("/register");
    // }

    // function goToCourse() {
    //     navigate(`course/${crouse.id}`)
    // }

    return (
        <div
            onClick={toggleCard}
            className={`transition-all cursor-pointer ${
                opened ? `grid-full` : `hover:scale-100 group`
            }`}
        >
            <div
                className={`transition-all w-full card-collapse ${
                    opened ? "opened flex" : ""
                }`}
            >
                <img
                    className={`card transition-all max-h-96 max-w-96 duration-500 ${
                        openedf
                            ? "max-h-52 max-w-52 lg:max-h-96 lg:max-w-96"
                            : ""
                    }`}
                    src="https://picsum.photos/300/300"
                />
                <div className="py-4 px-2">
                    <div className="font-bold group-hover:text-violet-800">
                        {course.title}
                    </div>
                    {opened && (
                        <div>
                            {course.description}
                            <button onClick={navigate(`/courses/${course.id}`)}>
                                Bekijk course
                            </button>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}

CourseCard.propTypes = {
    course: PropTypes.instanceOf(Course).isRequired,
};

export default CourseCard;
