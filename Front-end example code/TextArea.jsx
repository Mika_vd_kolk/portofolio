import React, {useState, useEffect} from "react";

function TextArea() {
    return (
        <div className="container w-full mx-auto">
            <div className="px-4 text-gray-800 leading-normal justify-center rounded">
                <div className="bg-indigo-50 border-2 border-black rounded shadow p-2 flex justify-center">
                    <textarea cols="100" rows="10" placeholder="Insert Message"></textarea>
                </div>
            </div>
        </div>
    );
}

export default TextArea;
