import axios from "axios";
import React, {useEffect, useState} from "react";
import {isAuthorized} from "../utils";

function EditThread({thread, user, course, setReadMode, setThread}) {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");

    useEffect(() => {
        setTitle(thread.title);
        setDescription(thread.description);
    }, [thread]);

    return (
        <div className="px-4 text-gray-800 leading-normal justify-center rounded mb-10 mt-10">
            <div className="bg-[#F7F7FF] rounded shadow-md p-10">
                <input
                    className="text-gray-500 border-2 border-violet-300 focus:border-purple-600 w-full py-2 px-3"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                />
                <br/>
                <br/>
                <textarea
                    className="text-gray-500 border-2 border-violet-300 focus:border-purple-600 w-full"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    cols="40"
                    rows="8"
                />
                <hr className="border-violet-300"/>
                <p className="uppercase text-sm text-indigo-500">
                    {thread?.user.first_name}&nbsp;{thread?.user.last_name}
                </p>
                <div className="flex justify-end">
                    {user.id === thread.user.id &&
                        <button
                            onClick={updateThread}
                            className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-500 text-base font-medium text-white hover:bg-indigo-600 sm:ml-3 sm:w-auto sm:text-sm">
                            Opslaan
                        </button>
                    }
                    {(user.id === thread.user.id || isAuthorized(user)) &&
                        <button
                            onClick={setReadMode}
                            className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-500 text-base font-medium text-white hover:bg-red-600 sm:ml-3 sm:w-auto sm:text-sm">
                            Annuleren
                        </button>
                    }
                </div>
            </div>
        </div>
    );

    async function updateThread() {
        const threadResponse = await axios.put(`thread/${thread.id}`, {
            title,
            description,
            user_id: user.user_id,
            course_id: course.course_id
        });
        setThread(prevThread => ({...threadResponse.data, user, posts: prevThread.posts}));
        setReadMode();
    }
}

export default EditThread;
