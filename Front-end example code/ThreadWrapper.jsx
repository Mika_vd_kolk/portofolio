import React, {useState} from "react";
import Wrapper from "./Wrapper";
import {Thread} from "../pages/Thread";
import EditThread from "./EditThread";

export function ThreadWrapper() {
    const [isEditMode, setIsEditMode] = useState(false);

    return (
        <Wrapper>
            {isEditMode ?
                <EditThread/>
                :
                <Thread/>
            }

            <div className="flex justify-end">
                <button
                    onClick={onClick}
                    className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-500 text-base font-medium text-white hover:bg-indigo-600 sm:ml-3 sm:w-auto sm:text-sm"
                >
                    {isEditMode ? 'Opslaan' : 'Wijzigen'}
                </button>
                <button
                    className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-500 text-base font-medium text-white hover:bg-red-600 sm:ml-3 sm:w-auto sm:text-sm">
                    {isEditMode ? 'Annuleren' : 'Verwijderen'}
                </button>
            </div>
        </Wrapper>
    );

    function onClick() {
        if (isEditMode) {
            // opslaan
            return;
        }
        setIsEditMode(true);
    }
}
