import React, {useState, useEffect, createContext} from "react";
import {useNavigate} from "react-router-dom";

import Nav from "./Nav";
import {User} from "../models/user";
import axios from "axios";

export const UserContext = createContext(new User());

export default function (props) {
    const [redirect, setRedirect] = useState(false);
    const [user, setUser] = useState(new User());

    const navigate = useNavigate();

    useEffect(() => {
        const getUser = async () => {
            try {
                const {data} = await axios.get("user");
                setUser(
                    new User(data.id, data.first_name, data.last_name, data.email, data.role.name)
                );
            } catch (error) {
                setRedirect(true);
            }
        };

        getUser();
    }, []);

    if (redirect) {
        navigate("/login");
    }

    return (
        <UserContext.Provider value={user}>

            <Nav/>

            <main className="ml-20 p-8 h-screen overflow-x-hidden ">{props.children}</main>
        </UserContext.Provider>
    );
}
