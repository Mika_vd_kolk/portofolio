import React, {useState, useEffect} from "react";
import axios from "axios";

function CreatePost({threadId, setThread, user}) {
    const [description, setDescription] = useState("");
    const [isPosting, setIsPosting] = useState(false);

    const createPost = async (e) => {
        e.preventDefault();
        setIsPosting(true);
        const post = await axios.post("post", {
            description: description,
            thread_id: threadId,
            user_id: user.user_id,
        });
        console.log(post.data);
        setDescription("");
        setThread(prevThread => ({...prevThread, posts: [...prevThread.posts, {...post.data, user}]}));
        setIsPosting(false);
    };

    return (
        <div className="p-10 mb-10">
            <h5 className="text-2xl">
                Plaats een reactie
            </h5>
            <hr className="border-violet-300"/>
            <br/>
            <form onSubmit={createPost}>
                    <textarea
                        className="text-sm text-gray-500 border-2 border-violet-300 focus:border-purple-600 w-full"
                        rows="8"
                        cols="40"
                        required
                        placeholder="Typ hier"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}>
                    </textarea>
                <br/>
                <p className="font-italic uppercase text-indigo-500">
                </p>
                <button
                    type="submit"
                    disabled={isPosting}
                    className="mt-3 w-full inline-flex justify-center rounded-md border bg-indigo-600 shadow-sm px-4 py-2 text-base font-medium text-white sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm hover:bg-indigo-700">
                    Plaatsen
                </button>
            </form>
        </div>
    )
}

export default CreatePost;
