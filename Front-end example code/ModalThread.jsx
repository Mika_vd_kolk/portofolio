import {Fragment, useEffect, useRef, useState} from 'react'
import {Dialog, Transition} from "@headlessui/react";
import axios from "axios";

function ModalThread({setOpen, open, userId, courseId, setCourse}) {
    const [title, setTitle] = useState("");
    const [user, setUser] = useState();
    const [isCreating, setIsCreating] = useState(false);
    const [description, setDescription] = useState("");
    const cancelButtonRef = useRef(null)

    useEffect(() => {
        axios.get('user').then(res => {
            const User = res.data;
            setUser(User);
            console.log(User);
        })
    }, [])

    const createThread = async (e) => {
        setIsCreating(true);
        const thread = await axios.post("thread", {
            title: title,
            description: description,
            user_id: userId,
            course_id: courseId
        });
        setOpen(false);
        setCourse(prevCourse => ({...prevCourse, threads: [...prevCourse.threads, {...thread.data, user}]}))
    };

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog as="div" className="relative z-10" initialFocus={cancelButtonRef} onClose={setOpen}>
                <Transition.Child
                    as={Fragment}
                    enter="ease-out duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"/>
                </Transition.Child>

                <div className="fixed z-10 inset-0 overflow-y-auto">
                    <div className="flex items-end sm:items-center justify-center min-h-full p-4 text-center sm:p-0">
                        <Transition.Child
                            as={Fragment}
                            enter="ease-out duration-300"
                            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                            enterTo="opacity-100 translate-y-0 sm:scale-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        >
                            <Dialog.Panel
                                className="relative bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:max-w-lg sm:w-full">
                                <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                    <div className="">
                                        <div className="mt-3 text-center sm:mt-0 sm:mx-4 sm:text-left">
                                            <Dialog.Title as="h3" className="text-xl font-bold leading-6 text-gray-900">
                                                Nieuwe thread aanmaken
                                            </Dialog.Title>
                                            <form onSubmit={createThread}>
                                                <div className="mt-2 form-group">
                                                    <div className="row">
                                                        <label className="font-bold text-indigo-600">Titel</label>
                                                        <br/>
                                                        <input
                                                            className="text-sm text-gray-500 border-2 border-violet-300 focus:border-purple-600 w-full"
                                                            placeholder="Titel"
                                                            value={title}
                                                            onChange={(e) => setTitle(e.target.value)}>
                                                        </input>
                                                        <br/>
                                                    </div>
                                                    <div className="row">
                                                        <label className="font-bold text-indigo-600">Tekst</label>
                                                        <br/>
                                                        <textarea
                                                            className="text-sm text-gray-500 border-2 border-violet-300 focus:border-purple-600 w-full"
                                                            rows="8"
                                                            cols="40"
                                                            placeholder="Typ hier je vraag of opmerking"
                                                            value={description}
                                                            onChange={(e) => setDescription(e.target.value)}>
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div className="bg-violet-100 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                                    <button
                                        type="button"
                                        className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 sm:ml-3 sm:w-auto sm:text-sm"
                                        onClick={() => setOpen(false)}
                                    >
                                        Cancel
                                    </button>
                                    <button disabled={isCreating}
                                            type="submit"
                                            className="mt-3 w-full inline-flex justify-center rounded-md border bg-indigo-600 shadow-sm px-4 py-2 text-base font-medium text-white sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                                            onClick={createThread}
                                    >
                                        Plaatsen
                                    </button>
                                </div>
                            </Dialog.Panel>
                        </Transition.Child>
                    </div>
                </div>
            </Dialog>
        </Transition.Root>
    )
}

export default ModalThread;
