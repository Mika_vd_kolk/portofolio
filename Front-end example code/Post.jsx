import EditPost from "./EditPost";
import {useEffect, useState} from "react";
import axios from "axios";
import {isAuthorized} from "../utils";

export function Post({post, user, setThread}) {
    const [isEditMode, setIsEditMode] = useState(false);
    const [isToggled, setIsToggled] = useState(false);

    useEffect(() => {
    }, [])

    console.log(isAuthorized(user));
    return (

        <div
            key={post.id}
            className="grid grid-flow-row auto-rows-max justify-content-sm-center px-4 text-gray-800 leading-normal rounded mb-5 columns-1"
        >
            {isEditMode ?
                <EditPost post={post} user={user} setThread={setThread}
                          setReadMode={() => setIsEditMode(false)}/>
                :
                <div className="bg-[#F7F7FF] rounded shadow-md py-5 px-10">
                    <h3 className="font-bold text-gray-500">{post.description}</h3>
                    <br/>
                    <hr className="border-violet-300"/>
                    <h2 className="uppercase text-sm text-indigo-500">{post.user.first_name}&nbsp;{post.user.last_name}</h2>
                    <div className="flex justify-end">
                        {user.id === post.user.id &&
                            <button
                                onClick={() => setIsEditMode(true)}
                                className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-500 text-base font-medium text-white hover:bg-indigo-600 sm:ml-3 sm:w-auto sm:text-sm"
                            >
                                Wijzigen
                            </button>
                        }
                        {(user.id === post.user.id || isAuthorized(user)) &&
                            <button
                                onClick={deletePost}
                                className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-500 text-base font-medium text-white hover:bg-red-600 sm:ml-3 sm:w-auto sm:text-sm">
                                Verwijderen
                            </button>
                        }
                    </div>

                </div>
            }
        </div>
    );


    async function deletePost() {
        const postResponse = await axios.delete(`post/${post.id}`);
        if (postResponse) {
            setThread(prevThread => ({...prevThread, posts: prevThread.posts.filter(p => p.id !== post.id)}))
        }
    }
}
